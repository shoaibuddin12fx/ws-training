<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ConsultingController extends Controller
{
    public function breadcrumbsection()
    {
        $obj=[
            'noshi',
        ];
        return self::success('Nosh',$obj);
    }
}
