<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseDetailController extends Controller
{
    public function breadcrumbsection()
    {
        $obj=[
            'noshi',
        ];
        return self::success('Nosh',$obj);
    }
    public function coursedetail()
    {
        $obj=[
            'noshi',
        ];
        return self::success('Nosh',$obj);
    }
}
