<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ws-Training') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
    <script>
        var laravel = @json([ 'baseURL' => url('/'), 'csrfToken' => csrf_token()  ])
    </script>

    <script>
        window._asset = '{{ asset('') }}';
    </script>
    <!-- Fonts -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/icofont.css" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    
    <link href="css/barfiller.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/settings/_mixin.css" rel="stylesheet">
    <link href="css/theme/_404-page.css" rel="stylesheet">
    <link href="css/theme/_blog-page.css" rel="stylesheet">
    <link href="css/theme/_contact-page.css" rel="stylesheet">
    <link href="css/theme/_course-details-page.css" rel="stylesheet">
    <link href="css/theme/_course-page.css" rel="stylesheet">
    <link href="css/theme/_login-page.css" rel="stylesheet">
    <link href="css/theme/_purchase.css" rel="stylesheet">

</head>
<body>

    <div id="app" class="main-outer">
        <router-view></router-view>
    </div>
</body>
</html>
