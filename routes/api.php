<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// test api
Route::prefix('lms')->group(function () {
    Route::get('abouttext', 'Frontend\AboutController@aboutText');
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\BankingController@breadcrumbsection');
    Route::get('course-category', 'Frontend\LayoutController@coursecategory');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    // BLog.vue
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\BlogController@breadcrumbsection');
    Route::get('blog-content', 'Frontend\BlogController@blogcontent');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //blogdetail
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\BlogDetailController@breadcrumbsection');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //buisness
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\BuisnessController@breadcrumbsection');
    Route::get('course-category', 'Frontend\LayoutController@coursecategory');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //cart
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\CartController@breadcrumbsection');
    Route::get('cart-section', 'Frontend\CartController@cartsection');
    Route::get('payment-section', 'Frontend\CartController@paymentsection');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //consulting
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\ConsultingController@breadcrumbsection');
    Route::get('course-category', 'Frontend\LayoutController@coursecategory');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //contact
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\ContactController@breadcrumbsection');
    Route::get('contact-info', 'Frontend\ContactController@contactinfo');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //corporate
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\CorporateController@breadcrumbsection');
    Route::get('course-category', 'Frontend\LayoutController@coursecategory');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //course
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\CourseController@breadcrumbsection');
    Route::get('course-section', 'Frontend\CourseController@coursesection');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //coursedetail
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-section', 'Frontend\LayoutController@headersection');
    Route::get('breadcrumb-section', 'Frontend\CourseDetailController@breadcrumbsection');
    Route::get('course-detail', 'Frontend\CourseDetailController@coursedetail');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');
    //coursevideo
    Route::get('main-menu', 'Frontend\LayoutController@mainmenu');
    Route::get('header-video', 'Frontend\coursevideoController@headervideo');
    Route::get('course-video', 'Frontend\CoursevideoController@coursevideo');
    Route::get('CTA-section', 'Frontend\LayoutController@CTAsection');
    Route::get('footer-section', 'Frontend\LayoutController@footersection');


});
Route::get('aboutText', 'Frontend\AboutController@aboutText');
